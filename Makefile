server:
	go run cmd/main.go

# this requires "air" to be installed
# you can run `go install github.com/cosmtrek/air@latest` to get it
live:
	air 