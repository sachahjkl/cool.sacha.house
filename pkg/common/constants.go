package common

const (
	USER_KEY         = "user"
	USER_CONTEXT_KEY = "userJWT"
	USER_COOKIE_KEY  = "user"
	USER_CLAIM_KEY   = "userId"
)